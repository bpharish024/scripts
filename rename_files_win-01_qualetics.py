import os, sys
import glob
from zipfile import ZipFile
import shutil
FILE_TYPE = 'png'
FILENAME_SEPERATOR = '__00__'
OUTPUT_ZIP = '_new'

if len(sys.argv)==2:
    input_path = sys.argv[1]
    if not os.path.exists(input_path):
        print("Zip file not found!!! Give valid zip path.")
        sys.exit()
    try:
        with ZipFile(input_path, 'r') as zip:
            zip.extractall(os.path.basename(input_path).split('.')[0])
        if len(os.listdir(os.path.basename(input_path).split('.')[0]))==0:
            sys.exit()

    except Exception as e:
        print(e.__class__.__name__)
        print(input_path + " : Invalid zip file")
        sys.exit()
TEMP_FOLDER = input_path.split('.')[0]
print(TEMP_FOLDER)

def create_folder(OUTPUT_TYPE):
    if not os.path.exists(TEMP_FOLDER+OUTPUT_TYPE):
        os.makedirs(TEMP_FOLDER+OUTPUT_TYPE)


# list_info = [(root, files) for root, dirs, files in os.walk(TEMP_FOLDER) if glob.glob(root+"/*."+FILE_TYPE)]
list_info = [(root, files) for root, dirs, files in os.walk(TEMP_FOLDER)]
if len(list_info)==0: # if no files found to rename
    print("zip contains no files to rename")
    # shutil.rmtree(os.path.basename(TEMP_FOLDER).split('.')[0]+OUTPUT_ZIP) #delete temp folder
    shutil.rmtree(os.path.basename(TEMP_FOLDER).split('.')[0]) # delete extracted folder
    sys.exit()
# print(list_info)
for info in list_info:
    index = FILENAME_SEPERATOR.join(info[0].split('\\')[1:])
    for file in info[1]:
        folderType = file.split('.')[0] # top, bottom, left, right or front
        if not os.path.exists(TEMP_FOLDER+'_'+folderType):
            os.makedirs(TEMP_FOLDER+'_'+folderType)
        print(index+FILENAME_SEPERATOR+file)
        # print(info[0])
        os.rename(info[0]+'\\'+file, TEMP_FOLDER+'_'+folderType+'\\'+index+FILENAME_SEPERATOR+file)

shutil.rmtree(TEMP_FOLDER) # delete extracted folder
folderTypes = ['top', 'bottom', 'left', 'right', 'front']
for folderType in folderTypes:
    if os.path.exists(TEMP_FOLDER+'_'+folderType):
        shutil.make_archive(TEMP_FOLDER+'_'+folderType, 'zip', TEMP_FOLDER+'_'+folderType) #zip temp folder that contains all renamed files
        shutil.rmtree(TEMP_FOLDER+'_'+folderType) #delete temp folder