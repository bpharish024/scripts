import json, sys, re, os
import glob
from tqdm import tqdm
from zipfile import ZipFile
import shutil

OUTPUT_ZIP = '_edited'
META_FILE_TYPE = 'json'
# input_path = 'pilot_Lightmetrics1'
''' input zip that contains json files which needs to be validated
	this code will check each json file that contains exact 16 labels with no duplicates
	filenames will be listed in output terminal if any json contains more than or less than 16 labels or duplicates
	and if any lable name contains '_missing' then it will strip '_missing' from label name and add new field 'visible' and set to 0 else 1
	and gives out put as zip 
'''
if len(sys.argv)==2:
	input_path = sys.argv[1]
	if not os.path.exists(input_path):
		print("Zip file not found!!! Give valid zip path.")
		sys.exit()
	try:
		with ZipFile(input_path, 'r') as zip:
			zip.extractall(os.path.basename(input_path).split('.')[0])
	except Exception as e:
		print(e.__class__.__name__)
		print(input_path + " : Invalid zip file")
		sys.exit()

	json_files = [(root, files) for root, dirs, files in os.walk(os.path.basename(input_path).split('.')[0]) if glob.glob(root+"//*."+META_FILE_TYPE)]

	if len(json_files)==0:
		print("no json files found")
		shutil.rmtree(os.path.basename(input_path).split('.')[0]) #delete extracted folder
		sys.exit()
else:
	print("give directory path")
	print(">>> python "+sys.argv[0]+" 'directory path'")
	sys.exit()
# print(json_files)
invalidFilesList = []
for files in tqdm(json_files):
	for file in files[1]:
		ok = False
		if file == "info.json":
				continue
		try:
			check = []
			f = open(files[0]+'\\'+file, "r")
			data = json.loads(f.read())
			f.close()
			landmarksData = data['annotation']['data_annotation']['marker']
		except:
			if len(data['annotation']['data_annotation'])==0:
				print(file,": no annotation")
			else:
				print(file,"somthing went wrong")
			continue
		for landmark in landmarksData:
			check.append(landmark['classification_label'])
		# print(file,len(check))

		if len(check)>16: # invalid if more than 16 labels
			invalidFilesList.append(files[0]+'/'+file+" : contains more than 16 labels"len(check))
			f.close()
			continue
		if len(check)<16: # invalid if more than 16 labels
			invalidFilesList.append(files[0]+'/'+file+" : contains less than 16 labels",len(check))
			continue
		check_dups = set(check)
		if len(check_dups)<16:# invalid if less than 16 labels or contains duplicate labels
			invalidFilesList.append(files[0]+'/'+file+" : contains duplicates")
			continue
		if len(check)==16:
			check_final_dups = []
			for landmark in landmarksData:
				start_pos = landmark['classification_label'].find('_missing')
				if start_pos==-1: # if '_missing'.
					check_final_dups.append(landmark['classification_label'])
					print(landmark['classification_label'])
					landmark['visible'] = '1'
				else:
					landmark['classification_label'] = landmark['classification_label'][:start_pos].strip()
					check_final_dups.append(landmark['classification_label'][:start_pos].strip())
					print(landmark['classification_label'])
					landmark['visible'] = '0'
		final_dups = set(check_final_dups)
		if len(final_dups)<16:# invalid if less than 16 labels or contains duplicate labels
			invalidFilesList.append(files[0]+'/'+file+" : contains duplicates after removing miissing")
			continue
		json_object = json.dumps(data, indent = 4)    
		with open(files[0]+'\\'+file, "w") as outfile:
			outfile.write(json_object)
shutil.make_archive(os.path.basename(input_path).split('.')[0]+OUTPUT_ZIP, 'zip', os.path.basename(input_path).split('.')[0]) 
shutil.rmtree(os.path.basename(input_path).split('.')[0]) #delete extracted folder

if len(invalidFilesList)>0:
	print("\nInvalid Files :")
	for invalidFile in invalidFilesList:
		print(invalidFile)