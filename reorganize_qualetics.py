import os, sys, json
import glob
from zipfile import ZipFile
import shutil
import operator

META_FILE_TYPE = 'json' #file type in meta folder
DATA_FILE_TYPE = 'png'  #file type in data folder
FILENAME_SEPERATOR = '__00__'  #seperator used to rename file using directories name

OP_EXT = '_dir_filename' # output zip name

if len(sys.argv)==2:
    input_path = sys.argv[1]
    if not os.path.exists(input_path):
        print("Zip file not found!!! Give valid zip path.")
        sys.exit()
    try:
        with ZipFile(input_path, 'r') as zip:
            zip.extractall(os.path.basename(input_path).split('.')[0])

    except Exception as e:
        print(e.__class__.__name__)
        print(input_path + " : Invalid zip file")
        sys.exit()
else:
    print("invalid arguments. please follow the below command\n")
    print("'python "+sys.argv[0]+" path/to/zipName.zip'\n")
    sys.exit()

#checks jsons files are valid or not
invalidFilesList = []

READ_DATA_FOLDER = []
READ_META_FOLDER = []
READ_DATA_FOLDER = [(root, files) for root, dirs, files in os.walk(input_path.split('.')[0]) if glob.glob(root+"//*."+DATA_FILE_TYPE)]
READ_META_FOLDER = [(root, files) for root, dirs, files in os.walk(input_path.split('.')[0]) if glob.glob(root+"//*."+META_FILE_TYPE)]

OUTPUT_FOLDER = input_path.split('.')[0]+OP_EXT

if not os.path.exists(OUTPUT_FOLDER):
    os.makedirs(OUTPUT_FOLDER)

def reorganizing(FOLDER, TYPE):
    try:
        for files in FOLDER:
            # print(files)
            for file in files[1]:
                if file == "info.json":
                    continue
                dir_root = file.split(FILENAME_SEPERATOR)
                fileName = dir_root[-1].split('.')[0]+'_'+'_'.join(dir_root[:-2])+'.'+dir_root[-1].split('.')[1]
                dirs = '//'.join(dir_root[1:-1])
                # print(fileName)
                if not os.path.exists(OUTPUT_FOLDER+'//'+dirs+'//'+TYPE):
                    os.makedirs(OUTPUT_FOLDER+'//'+dirs+'//'+TYPE)
                shutil.move(files[0]+'//'+file, OUTPUT_FOLDER+'//'+dirs+'//'+TYPE+'//'+fileName)
        return True
    except :
        print("somthing went wrong during reorganizing !!!")
        if os.path.exists(input_path.split('.')[0]):
            shutil.rmtree(input_path.split('.')[0])
        if os.path.exists(OUTPUT_FOLDER):
            shutil.rmtree(OUTPUT_FOLDER)
        return False
#if success these two variables will get change to True
metaResult = False
dataResult = False

print('\n')
if len(READ_META_FOLDER):
    metaResult = reorganizing(READ_META_FOLDER, 'meta') #reorganize meta folder
print("reorganizing meta : "+str(metaResult))

if len(READ_DATA_FOLDER):
    dataResult = reorganizing(READ_DATA_FOLDER, 'data') #reorganize data folder
print("reorganizing data : "+str(dataResult))

if os.path.exists(input_path.split('.')[0]):
    shutil.rmtree(input_path.split('.')[0]) #remove extracted zip folder

if os.path.exists(OUTPUT_FOLDER):
    shutil.make_archive(OUTPUT_FOLDER, 'zip', OUTPUT_FOLDER) #zip reorganized folder
    shutil.rmtree(OUTPUT_FOLDER) #remove reorganized folder