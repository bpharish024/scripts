import requests
import os, glob, shutil
from pdf2image import convert_from_path

input_path = "udaan-pilot"

urls = []
'''
    this code will read each text file from a given directory(contains multiple text files)
    and store image url links from each text file(conatins multiple image url links)
    download images using request module one by one and if any image type is pdf then it will convert pdf to jpg format
    save images in folder(folder name will be same as particular text file name)
    and gives output as a zip
'''

txt_file_folder = [(root, files) for root, dirs, files in os.walk(os.path.basename(input_path).split('.')[0]) if glob.glob(root+"//*.txt")]

for info in txt_file_folder:
    for file in info[1]:
        with open(info[0]+'/'+file, 'r') as reader:
            line = reader.readline()
            while line != '':
                line = reader.readline()
                if len(line):
                    urls.append({'type':file.split('.')[0],'url':line.rstrip("\n").strip('[').strip(']')})

for url in urls:
    if not os.path.exists('./'+input_path+'/'+url['type']):
        os.mkdir('./'+input_path+'/'+url['type'])
    try:
        response = requests.get(url['url'])
        print(os.path.basename(url['url']))
        file = open(input_path+'/'+url['type']+'/'+os.path.basename(url['url']), "wb")
        file.write(response.content)
        file.close()
    except:
        print("somthing went wrong during downloading",url['url'])
        continue

    if not url['url'].find('.pdf')== -1:
        try:
            images = convert_from_path(input_path+'/'+url['type']+'/'+os.path.basename(url['url']))
            # print(len(images))
            for i in range(len(images)):
                # Save pages as images in the pdf
                images[i].save(input_path+'/'+url['type']+'/'+os.path.basename(url['url']).split('.')[0]+'.jpg', 'JPEG')
            os.remove(input_path+'/'+url['type']+'/'+os.path.basename(url['url']))
        except:
            print("somthing went wrong during pdf to jpg convertion",url['url'])
shutil.make_archive((input_path)+'_downloaded', 'zip', os.path.basename(input_path).split('.')[0])
